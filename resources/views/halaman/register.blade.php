<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas HTML pekan 1 hari 1</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label> <br><br>
        <input type="text" name="nama1"> <br><br>
        <label>Last Name</label> <br><br>
        <input type="text" name="nama2"> <br><br>
        <label>Gender :</label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality :</label> <br><br>
        <select name="kewarganegaraan">
            <option value="WNI">Indonesian</option>
            <option value="WNA">Singapore</option>
            <option value="WNA">Malaysia</option>
            <option value="WNA">Japanese</option>
        </select> <br><br>
        <label>Language Spoken :</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Arabic <br>
        <input type="checkbox">Japanese <br><br>
        <label>Bio :</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" name="submit" value="Sign Up">
    </form>
   
</body>
</html>