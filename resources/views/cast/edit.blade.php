@extends('layout.master')
@section('Judul')
    Edit Cast
@endsection
@section('content') 
   
        <h2>Tambah Data</h2>
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Nama Cast</label>
                    <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama Cast">
                    @error('title')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Umur Cast</label>
                    <input type="integer" name="umur" value="{{$cast->umur}}" class="form-control" placeholder="Masukkan Umur Cast">
                    @error('body')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Bio</label>
                    <textarea name="bio" class="form-control" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                    @error('body')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
   
@endsection